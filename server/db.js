const mongoose = require("mongoose");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://admin:Admin123@ds259528.mlab.com:59528/divyank");

const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error."));
db.once("open", () => console.log("MongoDB connection established."));

module.exports = mongoose;
