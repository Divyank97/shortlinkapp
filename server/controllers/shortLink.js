const shortid = require("shortid");
const ShortLink = require("../models/ShortLink");

function findShortLink(req, res) {
  return ShortLink.findOne({ shortID: req.params.id })
    .exec()
    .then(({ link }) => res.redirect(link))
    .catch(() => {
      res.status(500).json({
        success: false,
        params: {
          message: "Internal server error."
        }
      });
    });
}

function createShortLink(req, res) {
  return ShortLink.create({ ...req.body, shortID: shortid.generate() })
    .then(link => res.status(201).send(link))
    .catch(() => {
      res.status(500).json({
        success: false,
        params: {
          message: "Internal server error."
        }
      });
    });
}

module.exports = {
  findShortLink,
  createShortLink
};
