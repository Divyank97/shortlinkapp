const express = require("express");
const routes = require("./shortLink");

const router = express.Router();

router.use("/", routes);

module.exports = router;
