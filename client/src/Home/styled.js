import styled from "styled-components";

const H1 = styled.h1`
  font-size: ${({ fontSize }) => fontSize};
  color: #f6fbff;
  letter-spacing: 1px;
`;

const Wrapper = styled.div`
  overflow: hidden;
  width: 100%;
  margin: 0 auto;
  background: radial-gradient(circle, #22577d 0%, #063865 100%);
  min-height: 100vh;
`;

const TextContainer = styled.div`
  padding-top: 100px;
  text-align: center;
`;

const InputBox = styled.div`
  padding-top: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column;
`;

const Input = styled.input`
  border: none;
  border-radius: 14px;
  outline: 0;
  letter-spacing: 1px;
  font-size: 13px;
  font-family: "Lato", sans-serif;
  background-color: #366985;
  height: 44px;
  padding-left: 15px;
  width: 100%;
  max-width: 350px;
  color: #fafafa;
  ::placeholder {
    color: #fafafa;
  }
`;
const Anchor = styled.a`
  border: none;
  border-radius: 14px;
  outline: 0;
  font-family: "Lato", sans-serif;
  background-color: #366985;
  height: 44px;
  padding-left: 15px;
  width: 100%;
  max-width: 350px;
  color: #fafafa !important;
  justify-content: center;
  display: flex;
  align-items: center;
  font-size: 15px;
  letter-spacing: 1px;
  font-weight: 600;
`;
const Button = styled.button`
  padding: 15px;
  outline: 0;
  font-size: 15px;
  cursor: pointer;
  border-radius: 25px;
  background-color: #1576bd;
  color: #fafafa;
  border: 0;
  font-family: "Lato", sans-serif;

  font-weight: 500;
  letter-spacing: 1.14px;
  line-height: 16px;
  text-align: center;
`;
export { H1, Wrapper, TextContainer, InputBox, Input, Button, Anchor };
