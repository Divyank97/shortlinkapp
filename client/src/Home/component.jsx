import React from "react";
import ReactLoading from "react-loading";
import {
  H1,
  Wrapper,
  TextContainer,
  InputBox,
  Input,
  Button,
  Anchor
} from "./styled";

const Home = ({
  loading,
  generator,
  url,
  shortUrl,
  onChange,
  onSubmit
}) => (
  <Wrapper>
    <TextContainer>
      <H1 fontSize="36px">Short Url Generator</H1>
    </TextContainer>
    <InputBox>
      <H1 fontSize="18px"> Enter Your Url</H1>
      <Input name="url" type="text" onChange={onChange} value={url} />
    </InputBox>
    <InputBox>
      <Button onClick={onSubmit}>Generate Url</Button>
    </InputBox>
    {loading && (
      <InputBox>
        <ReactLoading
          type="spinningBubbles"
          color="#fafafa"
          height={50}
          width={50}
        />
      </InputBox>
    )}
    {generator && (
      <InputBox>
        <H1 fontSize="18px"> Generated Short Link</H1>
        <Anchor href={`${process.env.REACT_APP_API_HOST}/${shortUrl}`}>
          {process.env.REACT_APP_API_HOST}/{shortUrl}
        </Anchor>
      </InputBox>
    )}
  </Wrapper>
);

export default Home;
