import React, { Component } from "react";
import Home from "./component";
import validator from "validator";
import swal from "sweetalert";
const axios = require("axios");

class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      url: "",
      shortUrl: "",
      generator: false,
      loading: false
    };
  }

  onChange = e => {
    this.setState({ url: e.target.value });
    //console.log(e.target.value);
  };

  onSubmit = () => {
    const { url, shortUrl } = this.state;
    if (url === "" || !validator.isURL(url)) {
      swal("Please enter a valid URL.", "", "error");
    } else {
      this.setState({ loading: true });
      axios
        .post(process.env.REACT_APP_API_HOST, {
          link: url
        })
        .then(({ data }) => {
          console.log(data);
          this.setState({ shortUrl: data.shortID });
          console.log(shortUrl);
          this.setState({ generator: true, loading: false });
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  };

  render() {
    const { url, shortUrl, loading, generator } = this.state;

    return (
      <Home
        loading={loading}
        shortUrl={shortUrl}
        url={url}
        onChange={this.onChange}
        onSubmit={this.onSubmit}
        generator={generator}
      />
    );
  }
}

export default HomePage;
